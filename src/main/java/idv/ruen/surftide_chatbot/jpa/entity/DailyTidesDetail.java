package idv.ruen.surftide_chatbot.jpa.entity;

import idv.ruen.surftide_chatbot.jpa.dto.Tide;

import java.util.List;

public class DailyTidesDetail {
	private String tide_diff;
	private List<TideDetail> tides;

	public String getTide_diff() {
		return tide_diff;
	}

	public void setTide_diff(String tide_diff) {
		this.tide_diff = tide_diff;
	}

	public List<TideDetail> getTides() {
		return tides;
	}

	public void setTides(List<TideDetail> tides) {
		this.tides = tides;
	}
}
