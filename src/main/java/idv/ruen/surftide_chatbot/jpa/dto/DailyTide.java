package idv.ruen.surftide_chatbot.jpa.dto;

import java.time.LocalDate;
import java.util.List;

public class DailyTide {
	private String location;
	private String tide_diff;
	private LocalDate dt;
	private List<Tide> tides;

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getTide_diff() {
		return tide_diff;
	}

	public void setTide_diff(String tide_diff) {
		this.tide_diff = tide_diff;
	}

	public LocalDate getDt() {
		return dt;
	}

	public void setDt(LocalDate dt) {
		this.dt = dt;
	}

	public List<Tide> getTides() {
		return tides;
	}

	public void setTides(List<Tide> tides) {
		this.tides = tides;
	}
}
