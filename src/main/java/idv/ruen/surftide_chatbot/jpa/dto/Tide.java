package idv.ruen.surftide_chatbot.jpa.dto;

import java.time.LocalDateTime;

public class Tide {
	private String tide_height;
	private String tide_status;
	private String tide_dt;

	public String getTide_height() {
		return tide_height;
	}

	public void setTide_height(String tide_height) {
		this.tide_height = tide_height;
	}

	public String getTide_status() {
		return tide_status;
	}

	public void setTide_status(String tide_status) {
		this.tide_status = tide_status;
	}

	public String getTide_dt() {
		return tide_dt;
	}

	public void setTide_dt(String tide_dt) {
		this.tide_dt = tide_dt;
	}
}
