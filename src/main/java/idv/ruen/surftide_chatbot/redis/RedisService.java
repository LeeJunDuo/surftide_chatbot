package idv.ruen.surftide_chatbot.redis;

import idv.ruen.surftide_chatbot.jpa.dto.DailyTide;

import java.util.List;

public interface RedisService {
	void set(String key, String value);

	String getStr(String key);

	void set(String key, DailyTide value);

	DailyTide getList(String key);

	List<String> getKeys();
//	<T> void setT(String key, List<T> v);
//	<T> List<T> getTList(String key);
}
