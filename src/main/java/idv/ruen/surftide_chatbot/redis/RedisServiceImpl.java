package idv.ruen.surftide_chatbot.redis;

import idv.ruen.surftide_chatbot.jpa.dto.DailyTide;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

@Service("RedisService")
public class RedisServiceImpl implements RedisService{
	@Autowired
	private StringRedisTemplate strTemplate;

	@Resource(name = "redisTemplate")
	private ListOperations<String, DailyTide> listOps;


//	@Resource(name = "redisTemplate")
//	pr

	@Override
	public void set(String key, String value) {
		strTemplate.boundValueOps(key).set(value);
	}

	@Override
	public String getStr(String key) {
		return strTemplate.boundValueOps(key).get();
	}

	@Override
	public void set(String key, DailyTide value) {
//		listTemplate.boundListOps(key).leftPush(value);
		listOps.leftPush(key, value);
	}

	@Override
	public DailyTide getList(String key) {
		return listOps.leftPop(key);
	}

	@Override
	public List<String> getKeys() {
		Set<String> keys = strTemplate.keys("*");
		List<String> val = new LinkedList<>();
		Iterator<String> it = keys.iterator();
		while (it.hasNext()) {
			val.add(it.next());
		}
		return val;
	}

//	@Override
//	public <T> void setT(String key, List<T> v) {
//		testTemplate.boundListOps(key).leftPush(v);
//	}
//
//	@Override
//	public <T> List<T> getTList(String key) {
//		return testTemplate.boundListOps(key).leftPop();
//	}

}
