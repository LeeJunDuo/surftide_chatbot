package idv.ruen.surftide_chatbot.controller;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/")
public class CommonController {

	@GetMapping(value = "hello", produces = "application/json")
	public ResponseEntity<Object> HelloSpoiler() {
		return new ResponseEntity("Hello spoiler", HttpStatus.OK);
	}

	@PostMapping(value = "", produces = "application/json")
	public ResponseEntity<Object> simplePostTest() {
		return new ResponseEntity("Post complete", HttpStatus.OK);
	}

}
