package idv.ruen.surftide_chatbot.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.linecorp.bot.model.event.Event;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.model.message.flex.component.Box;
import com.linecorp.bot.model.message.flex.component.FlexComponent;
import com.linecorp.bot.model.message.flex.component.Separator;
import com.linecorp.bot.model.message.flex.component.Text;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.container.BubbleStyles;
import com.linecorp.bot.model.message.flex.unit.FlexAlign;
import com.linecorp.bot.model.message.flex.unit.FlexFontSize;
import com.linecorp.bot.model.message.flex.unit.FlexLayout;
import com.linecorp.bot.model.message.flex.unit.FlexMarginSize;
import com.linecorp.bot.spring.boot.annotation.EventMapping;
import com.linecorp.bot.spring.boot.annotation.LineMessageHandler;
import idv.ruen.surftide_chatbot.jpa.dto.DailyTide;
import idv.ruen.surftide_chatbot.jpa.dto.Tide;
import idv.ruen.surftide_chatbot.jpa.entity.DailyTidesDetail;
import idv.ruen.surftide_chatbot.jpa.entity.TideDetail;
import idv.ruen.surftide_chatbot.redis.RedisService;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.time.temporal.ChronoUnit;
import java.util.LinkedList;
import java.util.List;

import static java.util.Arrays.asList;

@LineMessageHandler
public class EchoController {
	private final String MY_NAME = "@bot";

	@Autowired
	private RedisService redisService;

	@EventMapping
	public Message handleTextMessageEvent(MessageEvent<TextMessageContent> event) {
		System.out.println("event: " + event);
		String message = event.getMessage().getText();

		if (validateMessage(message)) {
			Message m = new TextMessage("找不到浪點～");
			String command = message.toLowerCase().replace(MY_NAME, "").trim();

			if (command.equals("浪點")) {
				StringBuilder sb = new StringBuilder();
				for (String key: redisService.getKeys()) {
					String[] k = key.split("_");
					sb.append(k[0]);
					sb.append('\n');
				}
				m = new TextMessage(sb.toString());
			} else {
				List<String> matchedKeys = new LinkedList<>();
				for (String spotName : redisService.getKeys()) {
					System.out.println("redis key: " + spotName);
					if (spotName.indexOf(command) > 0) {
						matchedKeys.add(spotName);
					}
				}

				if (matchedKeys.size() > 0) {
					DailyTide firstDailyTide = get_tides_from_redis(matchedKeys);
					m = genSpoilerMessage(firstDailyTide);
				}
			}
			return m;
		}
		return null;
	}

	private DailyTide get_tides_from_redis(List<String> keys) {
		String v = null;
		String k = null;
		for (String key : keys) {
			k = key;
			System.out.println("key: " + key);
			v = redisService.getStr(key);
			System.out.println("val: " + v);
		}
		return parseJsonStrToDailyTidesDetail(v, k);
	}

	private DailyTide parseJsonStrToDailyTidesDetail(String src, String key) {
		DailyTidesDetail dtd = null;
		ObjectMapper mapper = new ObjectMapper();
		try {
			dtd = mapper.readValue(src, DailyTidesDetail.class);
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println(dtd);
		return mapperDailyTide(dtd, key);
	}

	private DailyTide mapperDailyTide(DailyTidesDetail dtd, String key) {
		String[] metaInfo = key.split("_");
		List<Tide> tides = new LinkedList<>();
		for (TideDetail td : dtd.getTides()) {
			Tide t = new Tide();
			t.setTide_status(td.getStatus());
			t.setTide_height(String.valueOf(td.getHeight()));
			t.setTide_dt(td.getTs());
			tides.add(t);
		}

		DailyTide dailyTide = new DailyTide();
		dailyTide.setLocation(metaInfo[0]);
		dailyTide.setDt(LocalDate.parse(metaInfo[1], DateTimeFormatter.ISO_LOCAL_DATE));
		dailyTide.setTide_diff(dtd.getTide_diff());
		dailyTide.setTides(tides);
		return dailyTide;
	}

	private DailyTide genMockData() {
		LocalDateTime now = LocalDateTime.now();
		Tide t1 = new Tide();
		t1.setTide_dt(now.format(DateTimeFormatter.ofPattern("hh:mm")));
		t1.setTide_height("100");
		t1.setTide_status("滿潮");
		Tide t2 = new Tide();
		t2.setTide_dt(now.plus(6, ChronoUnit.HOURS).format(DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT)));
		t2.setTide_height("20");
		t2.setTide_status("乾潮");
		Tide t3 = new Tide();
		t3.setTide_dt(now.plus(12, ChronoUnit.HOURS).format(DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT)));
		t3.setTide_height("130");
		t3.setTide_status("滿潮");
		Tide t4 = new Tide();
		t4.setTide_dt(now.plus(18, ChronoUnit.HOURS).format(DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT)));
		t4.setTide_height("40");
		t4.setTide_status("乾潮");

		DailyTide mock = new DailyTide();
		mock.setLocation("金山");
		mock.setDt(LocalDate.now());
		mock.setTide_diff("大潮");
		mock.setTides(asList(t1,t2,t3,t4));
		return mock;
	}

	private FlexMessage genSpoilerMessage(DailyTide dailyTide) {

		final BubbleStyles bs =
				BubbleStyles.builder()
					.footer(new BubbleStyles.BlockStyle(null, true, null))
					.build();

		final Box location = genLocation(dailyTide);
		final Separator line = Separator.builder().margin(FlexMarginSize.XL).build();
		final Box tides = genTides(dailyTide.getTides());

		final Box content =
				Box.builder()
						.layout(FlexLayout.VERTICAL)
						.contents(asList(location, line, tides))
						.build();

		final Bubble bubble =
				Bubble.builder()
						.styles(bs)
						.body(content)
						.build();
		return new FlexMessage("Daily Tides", bubble);

	}

	private Box genLocation(DailyTide dailyTide) {
		final Box wrapper =
				Box.builder()
					.layout(FlexLayout.HORIZONTAL)
					.contents(genDailyInfo(dailyTide))
					.build();
		return Box.builder()
				.layout(FlexLayout.VERTICAL)
				.margin(FlexMarginSize.XXL)
				.spacing(FlexMarginSize.LG)
				.contents(asList(wrapper))
				.build();
	}

	private List<FlexComponent> genDailyInfo(DailyTide dailyTide) {
		final Text title =
				Text.builder()
					.text(dailyTide.getLocation())
					.weight(Text.TextWeight.BOLD)
					.size(FlexFontSize.XXL)
					.margin(FlexMarginSize.MD)
					.flex(0)
					.build();
		return asList(title, genMetaInfo(dailyTide.getTide_diff(), dailyTide.getDt()));
	}

	private Box genMetaInfo(String tideDiff, LocalDate dt) {
		final Text textTideDiff =
				Text.builder()
						.text(tideDiff)
						.weight(Text.TextWeight.BOLD)
						.color("#1DB446")
						.size(FlexFontSize.LG)
						.align(FlexAlign.END)
						.build();
		final Text textDT =
				Text.builder()
						.text(dt.format(DateTimeFormatter.ISO_LOCAL_DATE))
						.color("#aaaaaa")
						.size(FlexFontSize.XS)
						.align(FlexAlign.END)
						.wrap(true)
						.build();
		return Box.builder()
				.layout(FlexLayout.VERTICAL)
				.contents(asList(textTideDiff, textDT))
				.build();
	}
	private Box genTides(List<Tide> tides) {
		List<FlexComponent> tidesBox = new LinkedList<>();
		for (Tide t : tides) {
			Text status =
					Text.builder()
							.text(t.getTide_status())
							.size(FlexFontSize.SM)
							.color(t.getTide_status().startsWith("滿") ? "#FF0000" : "#0080FF")   // 滿#FF0000 |  乾#0080FF
							.flex(0)
							.build();
			System.out.println(t.getTide_dt());
			Text time =
					Text.builder()
							.text(t.getTide_dt())
							.size(FlexFontSize.SM)
							.color("#555555")
							.flex(0)
							.build();
			Text height =
					Text.builder()
							.text(t.getTide_height())
							.size(FlexFontSize.LG)
							.color("#111111")
							.align(FlexAlign.END)
							.build();
			tidesBox.add(Box.builder()
					.layout(FlexLayout.BASELINE)
					.contents(asList(status, time, height))
					.build());
		}
		return Box.builder()
				.layout(FlexLayout.VERTICAL)
				.margin(FlexMarginSize.XXL)
				.spacing(FlexMarginSize.SM)
				.contents(tidesBox)
				.build();
	}


	private Boolean validateMessage(String message) {
		return (message != null && message.trim().length() > 0);
//		return (message != null && message.trim().length() > 0 && message.toLowerCase().startsWith(MY_NAME));
	}

	@EventMapping
	public void handleDefaultMessageEvent(Event event) {
		System.out.println("event: " + event);
	}
}
