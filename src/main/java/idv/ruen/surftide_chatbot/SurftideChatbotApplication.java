package idv.ruen.surftide_chatbot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SurftideChatbotApplication {

	public static void main(String[] args) {
		SpringApplication.run(SurftideChatbotApplication.class, args);
	}

}
